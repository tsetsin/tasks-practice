    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '2,3,1',
    '5,2,3'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let strOne = gets();
  let strTwo = gets();
  let strArrOne = strOne.split(",");
  let strArrTwo = strTwo.split(",");
  let sorted = ""

  for (i = 0; i < strArrOne.length; i++) {
    sorted += strArrOne[i] + ","
    for (j = i; j < strArrTwo.length; j++) {
        sorted += strArrTwo[j] + ","
        break;
    }
  }

  print(sorted.slice(0, -1));