const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '8 3 6'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let str = gets();
  let strArr = [];
  strArr = str.split(" ");
  let numArr = [];
  
  for (i = 0; i < strArr.length; i++) {
    arr[i] = Number(strArr[i]);
  }
  
  print(Math.max(...arr));