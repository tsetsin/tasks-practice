    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '2 2',
    '0 1',
    '1 1',
    '1 0',
    '1 1',
    'P1 1 1',
    'P2 0 1',
    'P1 0 0',
    'P2 1 1',
    'P1 1 1',
    'P2 1 0',
    'END'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let size = gets();
  let arrSize = [];
  let arrAll = [];

  for (i = 0; i >= 0; i++) {
    arrAll[i] = gets();

    if (arrAll[i] === 'END') {
        arrAll.pop();
        break
    }
  }

  arrSize = size.split(' ')
  rows = Number(arrSize[0]);
  columns = Number(arrSize[1]);

  arrFirst = arrAll.splice(0, rows);
  arrSecond = arrAll.splice(0, rows);

  for (i = 0; i < arrFirst.length; i++) {
    arrFirst[i] = arrFirst[i].split(" ");
  }

  for (i = 0; i < arrSecond.length; i++) {
    arrSecond[i] = arrSecond[i].split(" ");
  }

  for (i = 0; i < arrAll.length; i++) {
    arrAll[i] = arrAll[i].split(" ");
  }

  for (i = 0 ; i < arrFirst.length; i++) {
    for (j = 0; j < arrFirst[i].length; j++) {
        arrFirst[i][j] = Number(arrFirst[i][j])
    }
  }

  for (i = 0 ; i < arrSecond.length; i++) {
    for (j = 0; j < arrSecond[i].length; j++) {
        arrSecond[i][j] = Number(arrSecond[i][j])
    }
  }

  for (i = 0 ; i < arrAll.length; i++) {
    for (j = 1; j < arrAll[i].length; j++) {
        arrAll[i][j] = Number(arrAll[i][j])
    }
  }

  for (i = 0 ; i < arrAll.length; i++) {
    if (arrAll[i][0] === 'P1' && arrSecond[arrSecond.length - 1 - arrAll[i][1]][arrAll[i][2]] === 0) {
        print('Missed');
        arrSecond[arrSecond.length - 1 - arrAll[i][1]][arrAll[i][2]] = 2;
    }

    else if (arrAll[i][0] === 'P1' && arrSecond[arrSecond.length - 1 - arrAll[i][1]][arrAll[i][2]] === 1) {
        print('Booom');
        arrSecond[arrSecond.length - 1 - arrAll[i][1]][arrAll[i][2]] = 3;
    }

    else if (arrAll[i][0] === 'P1' && arrSecond[arrSecond.length - 1 - arrAll[i][1]][arrAll[i][2]] > 1) {
        print('Try again!');
    }

    else if (arrAll[i][0] === 'P2' && arrFirst[arrAll[i][1]][arrAll[i][2]] === 0) {
        print('Missed');
        arrFirst[arrAll[i][1]][arrAll[i][2]] = 2;
    }

    else if (arrAll[i][0] === 'P2' && arrFirst[arrAll[i][1]][arrAll[i][2]] === 1) {
        print('Booom');
        arrFirst[arrAll[i][1]][arrAll[i][2]] = 3;
    }

    else if (arrAll[i][0] === 'P2' && arrFirst[arrAll[i][1]][arrAll[i][2]] > 1) {
        print('Try again!');
    }
  }

  let leftFirst = 0;
  let leftSecond = 0;

  for (i = 0; i < arrFirst.length; i++) {
    for (j = 0; j < arrFirst[i].length; j++) {
        if (arrFirst[i][j] === 1) {
            leftFirst++;
        }
    }
  }

  for (i = 0; i < arrSecond.length; i++) {
    for (j = 0; j < arrSecond[i].length; j++) {
        if (arrSecond[i][j] === 1) {
            leftSecond++;
        }
    }
  }

  print(leftFirst + ':' + leftSecond);