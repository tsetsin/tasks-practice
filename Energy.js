    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '3621'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let str = gets();
  let strArr = str.split("");
  let numArr = [];
  let sumOdd = 0;
  let sumEven = 0;

  for (i = 0; i < strArr.length; i++) {
    let n = Number(strArr[i]);
    numArr.push(n);
  }

  for (i = 0; i < numArr.length; i++) {
    if(numArr[i] % 2 == 0) {
        sumEven += numArr[i]
    }
    else{
        sumOdd += numArr[i] 
    }
  }
  
  if (sumEven > sumOdd) {
    print(sumEven + " energy drinks")
  }
  if (sumEven < sumOdd) {
    print(sumOdd + " cups of coffee")
  }
  if (sumEven == sumOdd) {
    print(sumOdd + " of both")
  }