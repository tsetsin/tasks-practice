    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '5'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let number = +gets();
  
  switch (number) {
    case 0:
      text = "zero";
      break;
    case 1:
      text = "one";
      break;
    case 2:
      text = "two";
      break;
    case 3:
      text = "three";
      break;
    case 4:
      text = "four";
      break;
    case 5:
      text = "five";
      break;
    case 6:
      text = "six";
      break;
    case 7:
      text = "seven";
      break;
    case 8:
      text = "eight";
      break;
    case 9:
      text = "nine";
      break;   
    default:
      text = "not a digit";
  }

  print(text);