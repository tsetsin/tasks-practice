    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '3'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let size = +gets();
    
  for (i = 0; i < size; i++) { 
    let str = ""

    for (j = i; j < size + i; j++) { 
        str += j + " ";
      }
    
      print(str.slice(0, -1));

  }
  
  