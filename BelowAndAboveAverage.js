    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '3,-12,0,0,13,5,1,0,-2'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let str = gets();
  let strArr = str.split(",");
  let numArr = [];
  let sum = 0;
  let above = ""
  let below = ""

  for (i = 0; i < strArr.length; i++) {
    let n = Number(strArr[i]);
    numArr.push(n);
  }

  for (i = 0; i < numArr.length; i++) {
    sum += numArr[i];
  }
  
  let avg = sum / numArr.length

  for (i = 0; i < numArr.length; i++) {
    if (numArr[i] > avg) {
    above += numArr[i] + ","
    }
  }

  for (i = 0; i < numArr.length; i++) {
    if (numArr[i] < avg) {
    below += numArr[i] + ","
    }
  }

  print("avg: " + avg.toFixed(2));
  print("below: " + below.slice(0, -1));
  print("above: " + above.slice(0, -1));