    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '9'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;
  
  let a = +gets();

  if (a >= 1 && a <= 3) {
    print(a * 10);
  }

  else if (a >= 4 && a <= 6) {
    print(a * 100);
  }

  else if (a >= 7 && a <= 9) {
    print(a * 1000);
  }

  else {
    print('invalid score');
  }