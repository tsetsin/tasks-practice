    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '2, 3, 1, 5, 6'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let str = gets();
  let strArr = str.split(", ");
  let strArr = [];
  let sorted = ""

  for (i = 0; i < strArr.length; i++) {
    strArr[i] = Number(strArr[i]);
  }
  
  strArr.sort(function(a, b){return b - a});
  
  for (i = 0; i < strArr.length; i++) {
    sorted += strArr[i] + ", "
  }

  print(sorted.slice(0, -2));