    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    'a',
    '7'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let f = gets();
  let r = +gets();
  
  if (((f == 'a' || f == 'c' || f == 'e' || f == 'g') && (r == 1 || r == 3 || r == 5 || r == 7 ))
  || ((f == 'b' || f == 'd' || f == 'f' || f == 'h') && (r == 2 || r == 4 || r == 6 || r == 8 ))) {
    print ('dark');
  } else {
    print ('light');
  }