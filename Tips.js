    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '12'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let a = +gets();
  
  let sum = Number(a * 1.1).toFixed(0)

  print(sum);