    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '3,-12,0,0,13,5,1,0,-2'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let str = gets();
  let strArr = str.split(",");
  let numArr = [];
  let sorted = ""

  for (i = 0; i < strArr.length; i++) {
    let n = Number(strArr[i]);
    numArr.push(n);
  }
  
  for (i = 0; i < numArr.length; i++) {
    if (numArr[i] < 0) {
    sorted += numArr[i] + ","
    }
  }

  for (i = 0; i < numArr.length; i++) {
    if (numArr[i] == 0) {
    sorted += numArr[i] + ","
    }
  }

  for (i = 0; i < numArr.length; i++) {
    if (numArr[i] > 0) {
    sorted += numArr[i] + ","
    }
  }

  print(sorted.slice(0, -1));