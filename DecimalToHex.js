    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '10'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let n = +gets();
  let hexArr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
  let length = n;
  let counter = 1;
  
  for (i = 0; i >= 0; i++) {
      if (length >= 16) {
        length = length / 16;
        counter++
      }
      else {
          break;
      }
  }
  
  result = '';
    
  for (i = 0; i < counter; i++) {
      result = hexArr [n % 16] + result;
      n = Math.floor(n / 16);
  }
  
  print(result);