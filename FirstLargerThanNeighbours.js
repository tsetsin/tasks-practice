    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '6',
    '-26 -25 -28 31 2 27'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let n = +gets();
  let arr = gets().split(' ');
  let result = 0;

  for (i = 1; i < arr.length - 1; i++) {
    if (Number(arr[i]) > Number(arr[i - 1]) && Number(arr[i]) > Number(arr[i + 1])) {
        print(i);
        break;
    }
}