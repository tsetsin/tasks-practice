    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '2346'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let str = gets();
  
  let digits = str.split('');
  let a = Number(digits[0]);
  let b = Number(digits[1]);
  let c = Number(digits[2]);
  let d = Number(digits[3]);

  print(a + b + c + d);