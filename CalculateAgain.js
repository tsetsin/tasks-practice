    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '100',
    '3'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let a = +gets();
  let b = +gets();
  a = BigInt(a);
  c = BigInt(b + 1);
  let result = 1n;
  let strResult = '';
  
  for (i = c; i <= a; i++) {
    result *= i;
  }

  strResult = String(result);
  print(strResult);