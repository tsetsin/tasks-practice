    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '10',
    '2',
    '1',
    '1',
    '2',
    '3',
    '3',
    '2',
    '2',
    '2',
    '1'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let size = +gets();
  let numArr = [];
  let longest = 0;
  let number = 0; 
  
  for (i = 0; i < size; i++) {
    arr[i] = +gets();
  }
    
  for (i = 0; i < arr.length - 1; i++) {
    if (arr[i] == arr[i + 1]) {
        number++;
    }
    if (arr[i] != arr[i + 1]) {
        number = 0;
    }
    if (number > longest) {
        longest = number;
    }
  }

  print (longest + 1);