    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '5'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let n = +gets();
  let s = ""

  for (i = 1; i < n + 1; i++) {
    if (i % 3 != 0 && i % 7 != 0) {
        s += i + " ";
      }
  }
  
  print(s.slice(0, -1));