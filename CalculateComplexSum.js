    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '3',
    '2'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let N = +gets();
  let x = +gets();
  let sum = 0;

  function factorial(N) 
  { 
    if (N == 0)
    {
    return 1;
    }
    return N * factorial(N-1);
  }
  
  for (i = 0; i < N + 1; i++) {
    sum += factorial(i) / Math.pow(x, i); 
  }

  print(sum.toFixed(5))