    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '0',
    '10,20,30,40,50',
    '2 forward 1',
    '2 backwards 1',
    '3 forward 2',
    '3 backwards 2',
    'exit',
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let start = +gets();
  let numbers = gets();
  let arrDir = [];
  let forwardSum = 0;
  let backwardsSum = 0;
  
  for (i = 0; i >= 0; i++) {
    arrDir[i] = gets();

    if (arrDir[i] === 'exit') {
        arrDir.pop();
        break
    }
  }

  let arrNum = numbers.split(',');

  for (i = 0; i < arrDir.length; i++) {
    arrDir[i] = arrDir[i].split(" ");
  }

  for (i = 0; i < arrNum.length; i++) {
    arrNum[i] = Number(arrNum[i])
  }

  for (i = 0 ; i < arrDir.length; i++) {
    for (j = 0; j < arrDir[i].length; j+=2) {
        arrDir[i][j] = Number(arrDir[i][j])
    }
  }

  for (i = 0; i < arrDir.length; i++) {
    if (arrDir[i][1] === "forward") {
        for (j = 0; j < (arrDir[i][0]); j++) {
        forwardSum += (arrNum[(start + (arrDir[i][2])) % arrNum.length]);
        start = start + (arrDir[i][2]) % arrNum.length;
        }
    }

    if (arrDir[i][1] === "backwards") {
        for (j = 0; j < (arrDir[i][0]); j++) {
        backwardsSum += (arrNum[(start - (arrDir[i][2] % arrNum.length) + arrNum.length) % arrNum.length]);
        start = (start - (arrDir[i][2] % arrNum.length) + arrNum.length) % arrNum.length;
        }
    }
  }

  print('Forward: ' + forwardSum);
  print('Backwards: ' + backwardsSum);