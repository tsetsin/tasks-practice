    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '1000'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let a = +gets();
  
  let sum1 = Number(a * 1.05).toFixed(2)
  let sum2 = Number(sum1 * 1.05).toFixed(2)
  let sum3 = Number(sum2 * 1.05).toFixed(2)

  print(sum1);
  print(sum2);
  print(sum3);