    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '8',
    '28 6 21 6 -7856 73 73 -56',
    '73'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;
  
  let length = +gets();
  let arr = gets().split(' ');

  let number = gets();
  let result = 0;

  for (i = 0; i < length; i++) {
      if (arr[i] == number) {
          result++;
      }
  }

  print(result);