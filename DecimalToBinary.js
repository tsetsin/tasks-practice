    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '36'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let n = +gets();

  if (n === 0) {
      print(0);
  }

  else {

  let length = Math.log2(n);
  result = '';
    
  for (i = 1; i <= length; i++) {
      if (n % 2 === 1) {
          result = "1" + result
      }

      else if (n % 2 === 0) {
        result = "0" + result
      }

      n = Math.floor(n / 2);
  }
  
  print('1' + result);

  }