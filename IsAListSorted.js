    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '5',
    '1,2,3,4,5',
    '1,2,8,9,9',
    '1,2,2,3,2',
    '1,2,2,2,5',
    '1,2,20,9,10'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let n = +gets();
  let strArr = [];
  let check = 1;
  
  for (i = 0; i < n; i++) {
    let m = gets();
    strArr.push(m);
  }

  for (i = 0; i < strArr.length; i++) {
    strArr[i] = strArr[i].split(",");
  }

   for (i = 0; i < strArr.length; i++) {

    for (j = 0; j < strArr[i].length - 1; j++) {
      if (Number(strArr[i][j]) <= Number(strArr[i][j + 1])) {
        check *= 1;
      }
      if (Number(strArr[i][j]) > Number(strArr[i][j + 1])) {
        check *= 0; 
      }
    }

    if (check == 1) {
        print("true")
    } else {
        print("false")
    }
    check = 1;
  }