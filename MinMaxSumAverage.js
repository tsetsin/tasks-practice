    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '3',
    '2',
    '5',
    '1'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let N = +gets();
  let number = 0
  let min = 10000
  let max = -10000

  for (i = 0; i < N; i++) {
    let n = +gets();
    number += n;
    if (n < min) {
        min = n
      }
    if (n > max) {
        max = n
      }
  }
  
  print("min=" + (min).toFixed(2));
  print("max=" + (max).toFixed(2));
  print("sum=" + (number).toFixed(2));
  print("avg=" + (number / N).toFixed(2));