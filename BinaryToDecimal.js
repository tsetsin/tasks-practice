    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '100100'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let str = gets();
  let arr = str.split("");
  result = 0;
    
  for (i = 0; i < arr.length; i++) {
    arr[i] = Number(arr[i]);
    result += arr[i] * Math.pow(2, arr.length - 1 - i)
  }
  
  print(result)