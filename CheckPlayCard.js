    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    'Q'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;
  
  let card = gets();

  if ((Number(card) >= 2 && Number(card) <= 10) || card == 'J' || card == 'Q' || card == 'K' || card == 'A') {
      print('yes ' + card);
  }

  else {
      print('no ' + card);
  }