    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '100'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let N = +gets();

  let primeFactors = [];
    while (N % 2 === 0) {
        primeFactors.push(2);
        N = N / 2;
    }
    
  let sqrtNum = Math.sqrt(N);
    for (i = 3; i <= sqrtNum; i++) {
        while (N % i === 0) {
            primeFactors.push(i);
            N = N / i;
        }
    }

    if (N > 2) {
        primeFactors.push(N);
    }

  for (i = 0; i < primeFactors.length; i++) {
     print (primeFactors[i]);
  }