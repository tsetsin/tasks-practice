    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '508'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let str = gets();
  let strArr = str.split("");
  let numArr = [];
  let sum = 0;
  let product = 1;
  
  for (i = 0; i < strArr.length; i++) {
    strArr[i] = Number(strArr[i]);
  }
  
  for (i = 0; i < strArr.length; i++) {
    if (strArr[i] < 2) {
        sum += strArr[i];
    }
    else {
        product *= strArr[i];
    }
  }

  if(strArr[1] === 0) {
    print(strArr[0] + strArr[2]);
  }

  else if(strArr[1] === 1 && strArr[0] != 1 && strArr[2] != 1) {
    print(strArr[0] * strArr[2]);
  }

  else if (product === 1) {
    print(sum);
  }

  else {
    print(sum + product);
  }