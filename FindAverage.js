    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '3',
    '3',
    '3',
    '1'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let N = +gets();
  let sum = 0

  for (i = 1; i < N + 1; i++) {
    let n = +gets();
    sum += n;
    
  }
  
  print((sum / N).toFixed(2));