    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '-test',
    'el examen',
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let english = gets();
  let spanish = gets();
  let str = '';

  let englishArray = english.split('');
  let spanishArray = spanish.split('');

  for (i = 0; i < englishArray.length; i++){
    englishArray[i] = (englishArray[i].charCodeAt() - 97)
  }

  for (i = 0; i < spanishArray.length; i++){
    spanishArray[i] = (spanishArray[i].charCodeAt() - 97)
  }

  if (englishArray.length > spanishArray.length) {
    spanishArray.length = englishArray.length
  }

  if (englishArray.length < spanishArray.length) {
    englishArray.length = spanishArray.length
  }

  for (i = 0; i < englishArray.length; i++) {
    if (isNaN(englishArray[i]) === true) {
      str += String.fromCharCode(spanishArray[i] + 97)
    }
    else if (isNaN(spanishArray[i]) === true) {
      str += String.fromCharCode(englishArray[i] + 97)
    }
    else if (englishArray[i] === -65 || englishArray[i] === -52) {
      str += String.fromCharCode(englishArray[i] + 97)
    }
    else if (spanishArray[i] === -65 || spanishArray[i] === -52) {
      str += String.fromCharCode(spanishArray[i] + 97)
    }
    else
    str += String.fromCharCode(Math.abs(englishArray[i] - spanishArray[i]) + 97)
  }

  print(str);