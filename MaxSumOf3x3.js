    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '5 5',
    '1 1 3 3 5',
    '-6 -7 2 -3 -1',
    '3 0 -4 5 9',
    '7 -7 0 1 0',
    '-7 -6 -4 -4 9'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let size = gets().split(' ');
  let n = Number(size[0]);
  let m = Number(size[1]);
  let arr = [];
  let sum = 0;
  let result = -10000;

  for (i = 0; i < n; i++) {
    arr[i] = gets().split(' '); 
      for (j = 0; j < m; j++) {
          arr[i][j] = Number(arr[i][j]);
      }
  }

  for (i = 0; i < n - 2; i++) {
      for (j = 0; j < m - 2; j++) {
      sum = arr[i][j] + arr[i][j + 1] + arr[i][j + 2] + arr[i + 1][j] + arr[i + 1][j + 1] + arr[i + 1][j + 2] + arr[i + 2][j] + arr[i + 2][j + 1] + arr[i + 2][j + 2];
        if (sum > result) {
            result = sum;
        }
      }
  }

  print(result);