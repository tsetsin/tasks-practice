    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '5,3,2,1',
    '2'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let str = gets();
  let N = +gets();
  let strArr = str.split(",");
  let sorted = ""

  for (i = 0; i < strArr.length; i++) {
    if (i + (N % strArr.length) < strArr.length) {
    sorted += strArr[i + (N % strArr.length)] + ","
    } else if (i + (N % strArr.length) >= strArr.length) {
      sorted += strArr[i + (N % strArr.length) - strArr.length] + ","
    }
  }

  print(sorted.slice(0, -1));