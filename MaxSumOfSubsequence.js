const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '10',
    '2',
    '3',
    '-6',
    '-1',
    '2',
    '-1',
    '6',
    '4',
    '-8',
    '8'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let size = +gets();
  let numArr = [];
  let maxSum = 0; 
  
  for (i = 0; i < size; i++) {
    strArr[i] = +gets();
  }
    
  for (i = 0; i < strArr.length; i++) {
    let sum = 0;
      for (j = i; j < strArr.length; j++) {
        sum += strArr[j];
        if (sum > maxSum) {
          maxSum = sum;
        }
      }
  }

  print (maxSum);