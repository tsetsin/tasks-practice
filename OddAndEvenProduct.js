    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '5', 
    '2 1 1 6 3',
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let N = +gets();
  let str = gets();
  let oddProduct = 1;
  let evenProduct = 1;
  
  let arr = str.split(' ');

  for (i = 0; i < arr.length; i+=2) {
    oddProduct *= arr[i];
  }

  for (i = 1; i < arr.length; i+=2) {
    evenProduct *= arr[i];
  }

  if (oddProduct == evenProduct) {
    print("yes " + oddProduct);
  } else {
    print("no " + oddProduct + " " + evenProduct);
  }