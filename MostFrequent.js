    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '13',
    '4',
    '1',
    '1',
    '4',
    '2',
    '3',
    '4',
    '4',
    '1',
    '2',
    '4',
    '9',
    '3'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let size = +gets();
  let numArr = [];
  let longest = 0;
  let number = 0;
  let element = 0;
  
  for (i = 0; i < size; i++) {
    strArr[i] = +gets();
  }

  strArr.sort(function(a, b){return b - a});
    
  for (i = 0; i < strArr.length - 1; i++) {
    if (strArr[i] == strArr[i + 1]) {
        number++;
    }
    if (strArr[i] != strArr[i + 1]) {
        number = 0;
    }
    if (number > longest) {
        longest = number;
        element = strArr[i];
    }
  }

  print (element + " (" + (longest + 1) + " times)");