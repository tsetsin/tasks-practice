    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '4 5'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  
  let str = gets();
  let number = str.split(' ');
  let rows = number[0];
  let cols = number[1];
  let arr = [];
  let result = '';
  let currentRow = 1;
  let currentCol = 1;
  let directionRow = 1;
  let directionCol = 1;

  function addStrings(strOne, strTwo){
    strOneA = strOne.split('').reverse();
    strTwoA = strTwo.split('').reverse();
    let output = '';
    let longer = Math.max(strOne.length, strTwo.length);
    let carry = false;
    for (let i = 0; i < longer; i++) {
      let result
      if (strOneA[i] && strTwoA[i]) {
        result = Number(strOneA[i]) + Number(strTwoA[i]);
  
      } else if (strOneA[i] && !strTwoA[i]) {
        result = Number(strOneA[i]);
  
      } else if (!strOneA[i] && strTwoA[i]) {
        result = Number(strTwoA[i]);
      }
  
      if (carry) {
        result += 1;
        carry = false;
      }
      if (result >= 10) {
        carry = true;
        output += result.toString()[1];
      }
      else {
        output += result.toString();
      }
    }
    output = output.split('').reverse().join('');
  
    if(carry) {
      output = '1' + output;
    }
    return output;
  }

  for (i = 0; i < rows; i++) {
      arr[i] = [];
      if (i == 0) {
        for (j = 1; j < cols; j++) {
            arr[i][0] = '1';
            arr[i][j] = String(addStrings(arr[i][j - 1], arr[i][j - 1]));
        }
      }
      
      else {
        for (j = 1; j < cols; j++) {
            arr[i][0] = String(addStrings(arr[i - 1][0], arr[i - 1][0]));
            arr[i][j] = String(addStrings(arr[i][j - 1], arr[i][j - 1])); 
        }
      }
  }

  result = arr[0][0];

  if (rows == 1 || cols == 1) {
      print(result);
  }
  else {

    while (true) {

        if (currentRow < rows - 1 && currentRow > 0 && currentCol < cols - 1 && currentCol > 0) {
            result = addStrings(result, arr[currentRow][currentCol]);
            currentRow += directionRow;
            currentCol += directionCol;
        }

        else if ((currentRow == rows - 1 || currentRow == 0) && (currentCol < cols - 1 && currentCol > 0)) {
            result = addStrings(result, arr[currentRow][currentCol]);
            directionRow = directionRow * (- 1);
            currentRow += directionRow;
            currentCol += directionCol;
        }

        else if ((currentRow < rows - 1 && currentRow > 0) && (currentCol == cols - 1 || currentCol == 0)) {
            result = addStrings(result, arr[currentRow][currentCol]);
            directionCol = directionCol * (- 1);
            currentCol += directionCol;
            currentRow += directionRow;
        }

        else {
            result = addStrings(result, arr[currentRow][currentCol]);
            break;
        }

    }

    print(result);

  }