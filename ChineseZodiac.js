    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '1982'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let year = +gets();

  let remainder = year % 12;
 
  switch (remainder) {
    case 0:
      text = "Monkey";
      break;
    case 1:
      text = "Rooster";
      break;
    case 2:
      text = "Dog";
      break;
    case 3:
      text = "Pig";
      break;
    case 4:
      text = "Rat";
      break;
    case 5:
      text = "Ox";
      break;
    case 6:
      text = "Tiger";
      break;
    case 7:
      text = "Hare";
      break;
    case 8:
      text = "Dragon";
      break;
    case 9:
      text = "Snake";
      break;
    case 10:
      text = "Horse";
      break;
    case 11:
      text = "Sheep";
      break;   
    default:
      text = "not a year";
  }

  print(text);