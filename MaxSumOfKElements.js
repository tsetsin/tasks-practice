const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '8',
    '3',
    '3',
    '2',
    '3',
    '-2',
    '5',
    '4',
    '2',
    '7'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let size = +gets();
  let number = +gets();
  let arr = [];
  let sum = 0;
  
  for (i = 0; i < size; i++) {
    strArr[i] = +gets();
  }
  
  strArr.sort(function(a, b){return b - a});
  
  for (i = 0; i < number; i++) {
    number += strArr[i];
  }

  print (number);