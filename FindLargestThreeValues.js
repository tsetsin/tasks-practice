    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '3',
    '3',
    '1',
    '2'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let N = +gets();
  let arr = [];

  for (i = 0; i < N; i++) {
    let n = +gets();
    arr.push(n);
  }
  
  arr.sort(function(a, b){return b - a});
  print(arr[0] + ", " + arr[1] + " and " + arr[2]);