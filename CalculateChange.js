    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '1.92',
    '5'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let bill = +gets();
  let cash = +gets();

  let change = cash - bill

  let lev = Math.floor(change);
  let fifty = Math.floor((change - lev).toFixed(2) / 0.5)
  let twenty = Math.floor((change - lev - fifty * 0.5).toFixed(2) / 0.2)
  let ten = Math.floor((change - lev - fifty * 0.5 - twenty * 0.2).toFixed(2) / 0.1)
  let five = Math.floor((change - lev - fifty * 0.5 - twenty * 0.2 - ten * 0.1).toFixed(2) / 0.05)
  let two = Math.floor((change - lev - fifty * 0.5 - twenty * 0.2 - ten * 0.1 - five * 0.05).toFixed(2) / 0.02)
  let one = Math.floor((change - lev - fifty * 0.5 - twenty * 0.2 - ten * 0.1 - five * 0.05 - two * 0.02).toFixed(2) / 0.01)
  
  if (lev > 0) {
    print (lev + ' x 1 lev');
  }

  if (fifty > 0) {
    print (fifty + ' x 50 stotinki');
  } 

  if (twenty > 0) {
    print (twenty + ' x 20 stotinki');
  } 

  if (ten > 0) {
    print (ten + ' x 10 stotinki');
  } 

  if (five > 0) {
    print (five + ' x 5 stotinki');
  } 

  if (two > 0) {
    print (two + ' x 2 stotinki');
  } 

  if (one > 0) {
    print (one + ' x 1 stotinka');
  } 