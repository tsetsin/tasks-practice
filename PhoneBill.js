    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '31',
    '115'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let msg = +gets();
  let min = +gets();

  let addMsg = msg - 20
  let addMin = min - 60

  if (addMsg < 0) {
    addMsg = 0;
  } 

  if (addMin < 0) {
    addMin = 0;
  } 

  print(addMsg + " additional messages for " + (addMsg * 0.06).toFixed(2) + " levas");
  print(addMin + " additional minutes for " + (addMin * 0.1).toFixed(2) + " levas");
  print(((addMsg * 0.06 + addMin * 0.1) * 0.2).toFixed(2) + " additional taxes");
  print((12 + ((addMsg * 0.06 + addMin * 0.1) * 1.2)).toFixed(2) + " total bill");