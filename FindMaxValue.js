    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '3',
    '1',
    '2',
    '3'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let N = +gets();
  let max = -200

  for (i = 0; i < N; i++) {
    let n = +gets();
    if (n > max) {
        max = n
      }
  }
  
  print(max);