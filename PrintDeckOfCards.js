    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '5'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let endCard = gets();
  let endCardNum = 0;
  let str = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];

  if (endCard == 2 || endCard == 3 || endCard == 4 || endCard == 5 || endCard == 6 || endCard == 7 || endCard == 8 || endCard == 9 || endCard == 10) {
    endCardNum = Number(endCard);
  } else if (endCard == 'J') {
    endCardNum = 11;
  } else if (endCard == 'Q') {
    endCardNum = 12;
  } else if (endCard == 'K') {
    endCardNum = 13;
  } else {
    endCardNum = 14;
  }

  for (i = 0; i < endCardNum - 1; i++) { 
    print(str[i] + " of spades, " + str[i] + " of clubs, " + str[i] + " of hearts, " + str[i] + " of diamonds");
  }