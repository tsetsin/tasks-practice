    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '8',
    '7',
    '3',
    '2',
    '3',
    '5',
    '2',
    '2',
    '4'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let size = +gets();
  let numArr = [];
  let longest = 0;
  let number = 0; 
  
  for (i = 0; i < size; i++) {
    strArr[i] = +gets();
  }
    
  for (i = 0; i < strArr.length - 1; i++) {
    if (strArr[i] < strArr[i + 1]) {
        number++;
    }
    if (strArr[i] >= strArr[i + 1]) {
        number = 0;
    }
    if (number > longest) {
        longest = number;
    }
  }

  print (longest + 1);