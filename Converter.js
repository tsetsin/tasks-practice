    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '1'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let a = +gets();
  
  let km = a * 1.6
  let ratio = 100 / km
  let consumption = Math.floor(ratio * 4.54);

  print (consumption + " litres per 100 km");