    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '3'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let dy = +gets();
  
  if (dy < 3) {
    print (dy * 10);
  } else {
    print (20 + (dy - 2) * 4);
  }