    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '17'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let n = +gets();
  let str = "";

  const isPrime = n => {
    if (n % 2 === 0) return (n === 2);
    if (n % 3 === 0) return (n === 3);
    let m = Math.sqrt(n);
    for (i = 5; i <= m; i += 6) {
        if (n % i === 0) return false;
        if (n % (i + 2) === 0) return false;
    }
    return true
  }  
  
  for (j = 0; j <= n; j++) {
    if (isPrime(j) == true) {
        str += String(j) + " ";
    }
  }

  print(str.slice(0, -1));