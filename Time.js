    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '4',
    '3',
    '11',
    '40'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let d = +gets();
  let h = +gets();
  let m = +gets();
  let s = +gets();

  let sum = 24 * 60 * 60 * d + 60 * 60 * h + 60 * m + s
  
  print(sum);