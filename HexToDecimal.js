    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '7DD'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let str = gets();
  let arr = str.split("");
  let result = 0;

  for (i = arr.length - 1; i >= 0; i--) {
    if (arr[i] === "A") {
        result += 10 * Math.pow(16, arr.length - 1 - i);
    }

    else if (arr[i] === "B") {
        result += 11 * Math.pow(16, arr.length - 1 - i);
    }

    else if (arr[i] === "C") {
        result += 12 * Math.pow(16, arr.length - 1 - i);
    }

    else if (arr[i] === "D") {
        result += 13 * Math.pow(16, arr.length - 1 - i);
    }

    else if (arr[i] === "E") {
        result += 14 * Math.pow(16, arr.length - 1 - i);
    }

    else if (arr[i] === "F") {
        result += 15 * Math.pow(16, arr.length - 1 - i);
    }

    else {
        result += Number(arr[i]) * Math.pow(16, arr.length - 1 - i);
    }
  }  

  print(result);