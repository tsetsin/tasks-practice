    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    'integer',
    '2'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let type = gets();
  if (type === 'integer') {
      int = +gets();
      print(int + 1);
  }

  else if (type === 'real') {
    double = +gets();
    print((double + 1).toFixed(2));
  }

  else if (type === 'text') {
    str = gets();
    print(str + '*');
  }