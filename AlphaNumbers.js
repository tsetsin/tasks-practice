    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '186',
    '144',
    '782',
    '764',
    '882',
    '909',
    '511'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  arr = [];
  arrSplit = [];
  
  for (i = 0; i < 7; i++) {
    arr[i] = gets();
  }  

  for (i = 0; i < arr.length; i++) {
    arrSplit[i] = arr[i].split("");
  }

  for (i = 0; i < arrSplit.length; i++) {
    for (j = 0; j < arrSplit[i].length; j++) {
        arrSplit[i][j] = Number(arrSplit[i][j]);
    } 
  }

  for (i = 0; i < arrSplit.length; i++) {
    arrSplit[i].sort(function(a, b){return b - a});
  }

  for (i = 0; i < arrSplit.length; i++) {
    if (arrSplit[i][0] - arrSplit[i][2] > (arrSplit[i][0] + arrSplit[i][1] + arrSplit[i][2]) % 10) {
        print(arr[i]);
    }  
  }