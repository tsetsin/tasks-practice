    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '28,1,45,255'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;
  
  let arrDec = gets().split(',');
  let arrBin = arrDec.slice();
  let result = '';

  for  (let i = 0; i < arrBin.length; i++) {
  
    for (let j = 0; j < 8; j++) {
        if (arrBin[i] % 2 === 1) {
            result = "1" + result
        }

        else if (arrBin[i] % 2 === 0) {
            result = "0" + result
        }

    arrBin[i] = Math.floor(arrBin[i] / 2);
    }

    arrBin[i] = result;
    result = '';
  }
  
  let arrBinCut = arrBin.slice();

  for (let i = 0; i < arrBinCut.length; i++) {
      if (i % 2 === 0) {
          for (let j = 0; j < 8; j += 2) {
              result = result + arrBinCut[i].charAt(j + 1);
          }
      }

      if (i % 2 != 0) {
        for (let j = 0; j < 8; j += 2) {
            result = result + arrBinCut[i].charAt(j);
        }
      }

      arrBinCut[i] = result;
      result = '';
  }

  for (let i = 0; i < arrBinCut.length; i++) {
      result += arrBinCut[i];
  }

  print(result);