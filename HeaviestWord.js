    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '5',
    'telerik',
    'alpha',
    'java',
    'Spring',
    'nodeJS'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let n = +gets();
  let strArr = [];
  let sum = 0;
  let biggestSum = 0;
  let index = 0;

  for (i = 0; i < n; i++) {
    let m = gets();
    strArr.push(m);
  }
  
  for (i = 0; i < strArr.length; i++) {

    for (j = 0; j < strArr[i].length; j++)
      if ((strArr[i].charAt(j)).charCodeAt() > 96) {
        sum += ((strArr[i].charAt(j)).charCodeAt() - 96);
      }
      else if ((strArr[i].charAt(j)).charCodeAt() < 96) {
        sum += ((strArr[i].charAt(j)).charCodeAt() - 64) 
      }

    if (sum > biggestSum) {
        biggestSum = sum;
        index = i;
    }
    sum = 0
  }
  print(biggestSum + " " + strArr[index])