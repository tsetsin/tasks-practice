    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '5',
    '1',
    '1',
    '2',
    '2'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let n = +gets();
  let gs = +gets();
  let gl = +gets();
  let ps = +gets();
  let pl = +gets();
  let gResult = (n * gs) + (2 * gl);
  let pResult = (n * ps) + (2 * pl);

  if (gResult < pResult) {
    print ("George");
  }
  else if (pResult < gResult) {
    print ("Peter")
  }
  else {
    print ("Draw")
  }