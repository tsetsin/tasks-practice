    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '1,2,3,3,5'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let str = gets();
  let strArr = str.split(",");
  let numArr = [];
  let index = 0
  let result = ""

  for (i = 0; i < strArr.length; i++) {
    let n = Number(strArr[i]);
    numArr.push(n);
  }
  
  for (i = 1; i < numArr.length + 1; i++) {
    index = numArr.indexOf(i);
    if (index < 0) {
        result += i + ","
    }
  }

  print (result.slice(0, -1));