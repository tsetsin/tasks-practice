    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    '2',
    '50',
    '13.50'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let N = +gets();

  for (i = 1; i < N + 1; i++) {
    let n = +gets();
    print((n * 0.350000001).toFixed(2)); 
  }