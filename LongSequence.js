    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let a = 0;
  let b = -1;

  for (i = 0; i < 500; i++) {
    a = a + 2
    print(a);
    b = b - 2
    print(b);
  }