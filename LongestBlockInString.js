    const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  
  // this is the test
  const test = [
    'hoplaSSS'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let arr = gets().split('');
  let result = '';
  let maxLength = 1;
  let counter = 1;
  let char = arr[0];

  for (let i = 1; i < arr.length; i++) {
    if (arr[i] === arr[i - 1]) {
        counter++;
    }
    if (arr[i] != arr[i - 1]) {
        counter = 1;
    }
    if (counter > maxLength) {
        maxLength = counter;
        char = arr[i];
    }
  }

  for (let i = 0; i < maxLength; i++) {
    result += char;
  }
  
  print(result);